#include <SPI.h>

#define CS_PIN 2
#define LASER_PIN 3

union {
  unsigned int data_int;
  uint8_t data_bytes[2];
} a_command, b_command;

unsigned int tick=0;

void setup() {
  pinMode(CS_PIN,OUTPUT);
  pinMode(LASER_PIN,OUTPUT);
  digitalWrite(CS_PIN,HIGH);
  SPI.begin();
  Serial.begin(9600);
  laserOn(255);
}

void loop() {
  unsigned int a_value, b_value;
  uint16_t radius=1024;
  uint16_t del=1;
  
  uint16_t x,y;
  x = x_from_pol(2.0*PI/3+tick*2.0*PI/3600,radius*2/3);
  y = y_from_pol(2.0*PI/3+tick*2.0*PI/3600,radius*2/3);
  laserGoto(x,y);
  
  for(uint16_t b=0; b<256; b++){
    delay(del*4);
    laserOn(b);
  }
  for(uint16_t r=radius*2/3; r > 0; r--){
    delay(del);
    uint16_t x,y;
    x = x_from_pol(2.0*PI/3+tick*2.0*PI/3600,r);
    y = y_from_pol(2.0*PI/3+tick*2.0*PI/3600,r);
    laserGoto(x,y);
  }
  for(uint16_t r=0; r <radius; r++){
    delay(del);
    uint16_t x,y;
    x = x_from_pol(tick*2.0*PI/60,r);
    y = y_from_pol(tick*2.0*PI/60,r);
    laserGoto(x,y);
  }

//  for(uint16_t theta=0; theta < 2.0*PI*radius; theta ++){
//    delay(del);
//    uint16_t x,y;
//    x = x_from_pol(tick*2.0*PI/60+theta/(radius*1.0),radius);
//    y = y_from_pol(tick*2.0*PI/60+theta/(radius*1.0),radius);
//    laserGoto(x,y);
//  }
  for(uint16_t b=255; b>0; b--){
    delay(del*4);
    laserOn(b);
  }
//  laserGoto(sin(tick*2.0*PI/360)*2047+2048,cos(tick*2.0*PI/360)*2047+2048);
  tick += 1;
  
}

uint16_t x_from_pol(float theta, float r){
  return (uint16_t) (-1*r*cos(theta)+2048);
}

uint16_t y_from_pol(float theta, float r){
  return (uint16_t) (r*sin(theta)+2048);
}


void laserGoto(uint16_t x, uint16_t y){
  a_command.data_int=0;
  b_command.data_int=0;

  //Serial.println((uint16_t) (sin(millis()*2.0*PI/360)*2048+2048));
  //set which channel we're setting
  a_command.data_int |= 0<<15;
  b_command.data_int |= 1<<15;

  //set output gain. 1 -> 2.048V 0 -> 4.096
  a_command.data_int |= 0<<13;
  b_command.data_int |= 0<<13;

  
  //set shutdown bit
  a_command.data_int |= 1<<12;
  b_command.data_int |= 1<<12;

  //set value
  a_command.data_int |= x;
  b_command.data_int |= y;
  
  digitalWrite(CS_PIN,LOW);

  SPI.transfer(a_command.data_bytes[1]);
  SPI.transfer(a_command.data_bytes[0]);
  
  digitalWrite(CS_PIN,HIGH);
  
  digitalWrite(CS_PIN,LOW);
  
  SPI.transfer(b_command.data_bytes[1]);
  SPI.transfer(b_command.data_bytes[0]);

  digitalWrite(CS_PIN,HIGH);

}

void laserOn(uint8_t laserLevel){
  analogWrite(LASER_PIN,laserLevel);
}

void laserOff(){
  analogWrite(LASER_PIN,0);
}


